#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright © 2021 Taylor C. Richberger
# This code is released under the license described in the LICENSE file

from enum import Enum, auto

class Order(Enum):
    Chaotic = auto()
    Neutral = auto()
    Lawful = auto()

    def __str__(self) -> str:
        return self.name

class Morality(Enum):
    Evil = auto()
    Neutral = auto()
    Good = auto()

    def __str__(self) -> str:
        return self.name

class Alignment:
    def __init__(self, order: Order, morality: Morality):
        self._order = order
        self._morality = morality

    @property
    def order(self) -> Order:
        return self._order

    @property
    def morality(self) -> Morality:
        return self._morality

    def __eq__(self, other) -> bool:
        if not isinstance(other, Alignment):
            return False
        return self._order is other.order and self._morality is other.morality

    def __str__(self):
        if self._order is Order.Neutral and self._morality is Morality.Neutral:
            return 'Neutral'
        else:
            return f'{self._order} {str(self._morality).lower()}'

    def __hash__(self) -> int:
        return hash((self._order, self._morality))

# We also define all the predefined alignments as a convenience
ChaoticEvil = Alignment(Order.Chaotic, Morality.Evil)
ChaoticNeutral = Alignment(Order.Chaotic, Morality.Neutral)
ChaoticGood = Alignment(Order.Chaotic, Morality.Good)
NeutralEvil = Alignment(Order.Neutral, Morality.Evil)
NeutralNeutral = Alignment(Order.Neutral, Morality.Neutral)
# Neutral Neutral has a handful of different aliases
Neutral = NeutralNeutral
TrueNeutral = NeutralNeutral
NeutralGood = Alignment(Order.Neutral, Morality.Good)
LawfulEvil = Alignment(Order.Lawful, Morality.Evil)
LawfulNeutral = Alignment(Order.Lawful, Morality.Neutral)
LawfulGood = Alignment(Order.Lawful, Morality.Good)
