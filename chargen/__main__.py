#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright © 2021 Taylor C. Richberger
# This code is released under the license described in the LICENSE file

import locale
import argparse
from chargen.race import HillDwarf

def main():
    locale.setlocale(locale.LC_ALL, '')
    parser = argparse.ArgumentParser(description='DND SRD character generator')
    parser.add_argument('-V', '--version', action='version', version='0.1')
    args = parser.parse_args()
    race = HillDwarf()
    print(race.ability_increases)
    print(race.get_alignment())

if __name__ == '__main__':
    main()
