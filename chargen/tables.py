#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright © 2021 Taylor C. Richberger
# This code is released under the license described in the LICENSE file

languages = {
    'standard': {
        'common',
        'dwarvish',
        'elvish',
        'giant',
        'gnomish',
        'goblin',
        'halfling',
        'orc',
    },
    'exotic': {
        'abyssal',
        'celestial',
        'draconic',
        'deepspeech',
        'infernal',
        'primordial',
        'sylvan',
        'undercommon',
    }
}
