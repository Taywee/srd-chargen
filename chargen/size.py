#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright © 2021 Taylor C. Richberger
# This code is released under the license described in the LICENSE file

from enum import Enum, auto

# We leave the unused sizes out of this table from this small program
class Size(Enum):
    Small = auto()
    Medium = auto()

    def __str__(self) -> str:
        return self.name

Small = Size.Small
Medium = Size.Medium
