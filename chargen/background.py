#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright © 2021 Taylor C. Richberger
# This code is released under the license described in the LICENSE file

import random
from typing import Set
from chargen.equipment import Equipment
from chargen.tables import languages
from chargen import character

class Background:
    @property
    def skill_proficiencies(self) -> Set[str]:
        raise NotImplementedError()

    @property
    def languages(self) -> Set[str]:
        raise NotImplementedError()

    @property
    def equipment(self) -> Equipment:
        raise NotImplementedError()

    def get_proficiencies(self, character: 'character.Character') -> Set[str]:
        proficiencies = set()
        for skill in self.skill_proficiencies:
            proficiencies.add(f'skill:{skill}')

        for language in self.languages:
            proficiencies.add(f'language:{language}')

        return proficiencies

class Acolyte(Background):
    skill_proficiencies = frozenset({'insight', 'religion'})

    # Set in get_proficiencies instead
    languages = set()

    @property
    def equipment(self) -> Equipment:
        return Equipment({
            'holy symbol',
            random.choice(['prayer book', 'prayer wheel']),
            'incense stick * 5',
            'vestaments',
            'common clothes',
            'gold piece * 15',
        })

    def get_proficiencies(self, character: 'character.Character') -> Set[str]:
        proficiencies = super().get_proficiencies(character)
        language_table_proficiencies = {f'language:{language}' for language in languages['standard']}
        language_choices = language_table_proficiencies - character.proficiencies
        proficiencies |= set(random.sample(list(language_choices), 2))
        return proficiencies
