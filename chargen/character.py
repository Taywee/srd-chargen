#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright © 2021 Taylor C. Richberger
# This code is released under the license described in the LICENSE file

from chargen import race, class_, background

class Character:
    def __init__(self, race: 'race.Race', background: 'background.Background', class_: 'class_.Class'):
        self.proficiencies = race.get_proficiencies()
        self.proficiencies |= background.get_proficiencies(self)
        self.proficiencies |= class_.get_proficiencies(self)
