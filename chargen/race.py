from typing import Tuple, Mapping, Set, List
from collections import defaultdict
import random
from chargen.alignment import (
    Alignment,
    ChaoticEvil,
    ChaoticNeutral,
    ChaoticGood,
    NeutralEvil,
    NeutralNeutral,
    NeutralGood,
    LawfulEvil,
    LawfulNeutral,
    LawfulGood,
)
from chargen.size import Size

def clamp(value: int, min: int, max: int) -> int:
    '''Simply clamp a value to make sure it always is between min and max.
    
    This is more readable than an inline form.
    '''

    if value >= max:
        return max
    elif value <= min:
        return min
    else:
        return value

class Race:
    '''The common Race subclass.
    This is used to get the common data for generating a character from the race data.
    '''

    # Properties are presented here just to give proper type hinting.

    @property
    def age(self) -> Tuple[int, int]:
        '''The range for the race's age.'''
        raise NotImplementedError()

    @property
    def alignment(self) -> Mapping[Alignment, int]:
        '''The race alignment with weights.'''
        raise NotImplementedError()

    @property
    def height(self) -> Tuple[int, int]:
        '''The range for the race height in inches.'''
        raise NotImplementedError()

    @property
    def size(self) -> Size:
        '''The race size'''
        raise NotImplementedError()

    @property
    def speed(self) -> int:
        '''The race speed'''
        raise NotImplementedError()

    @property
    def features(self) -> Set[str]:
        '''The race-specific features'''
        raise NotImplementedError()

    @property
    def proficiencies(self) -> Set[str]:
        '''The race-specific proficiencies'''
        raise NotImplementedError()

    @property
    def inherent_languages(self) -> Set[str]:
        '''The set of languages members of the race are guaranteed to speak.'''
        raise NotImplementedError()

    @property
    def ability_increases(self) -> defaultdict[str, int]:
        '''The set of race ability score increases.
        This returns a defaultdict that defaults to 0.
        '''
        return defaultdict(lambda: 0)

    @staticmethod
    def rand_given_range(range: Tuple[int, int]) -> int:
        '''Get a random value from a range, given as a tuple of ints.

        The random value will be taken as a normal distribution, in which the
        range encompasses four standard deviations (each side of the range is
        two standard deviations from the average value).  This value will be
        clamped to the extremes.
        '''

        # The average is at the center of the range.
        average = (range[0] + range[1]) / 2

        # The range encompasses two standard deviations on either side of the
        # average.
        stddev = (range[1] - average) / 2
        value = int(random.normalvariate(average, stddev))

        return clamp(value, min=range[0], max=range[1])

    def get_age(self):
        return self.rand_given_range(self.age)

    def get_alignment(self):
        # This can more concisely be expressed with this:
        #
        #   alignments, weights = zip(*self.alignment.items())
        # 
        # But that's unclear and ugly.

        alignments: List[Alignment] = []
        weights: List[int] = []
        for alignment, weight in self.alignment.items():
            alignments.append(alignment)
            weights.append(weight)

        return random.choices(alignments, weights)[0]

    def get_height(self):
        return self.rand_given_range(self.age)

    def get_proficiencies(self):
        '''Get the full proficiency set for the race (including randomly-selected ones).

        This includes languages.
        '''

        proficiencies = set(self.proficiencies)
        for language in self.inherent_languages:
            proficiencies.add(f'language:{language}')
        return proficiencies

class Dwarf(Race):
    age = (50, 350)
    alignment = {
        LawfulGood: 30,
        NeutralGood: 20,
        LawfulNeutral: 20,
        LawfulEvil: 5,
        ChaoticGood: 5,
        NeutralNeutral: 3,
        NeutralEvil: 2,
        ChaoticNeutral: 2,
        ChaoticEvil: 1,
    }
    size = Size.Medium
    height = (48, 60)
    speed = 25
    features = frozenset({'Darkvision', 'Dwarven Resilience', 'Dwarven Combat Training', 'Stonecunning'})
    proficiencies = frozenset({
        'weapon:battleaxe',
        'weapon:handaxe',
        'weapon:lighthammer',
        'weapon:warhammer',
        'tool:smithtools',
        'tool:brewersupplies',
        'tool:masontools',
    })
    inherent_languages = frozenset({'common', 'dwarvish'})

    @property
    def ability_increases(self) -> Mapping[str, int]:
        '''The set of race ability score increases'''
        bonuses = super().ability_increases
        bonuses['constitution'] += 2
        return bonuses

class HillDwarf(Dwarf):
    @property
    def ability_increases(self):
        bonuses = super().ability_increases
        bonuses['wisdom'] += 1
        return bonuses
