#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright © 2021 Taylor C. Richberger
# This code is released under the license described in the LICENSE file

from typing import Iterable, Optional, Dict, Union, ItemsView, Iterator
import re

# Regex that detects patterns like *20 and * 5 at the end of an item expression.
_multiplierpattern = re.compile(r'(.+?)\s*\*\s*(\d+)$')

class Equipment:
    '''Simple equipment management class.

    This includes some nice conveniences, such as having default values, some
    fun default construction stuff, and allowing merging equipment bags.
    '''

    def __init__(self, initial: Optional[Union[Iterable[str], 'Equipment']] = None):
        self._contents: Dict[str, int] = {}

        if initial is not None:
            if isinstance(initial, Equipment):
                # Just copy the input equipment
                self._contents = dict(initial._contents)
            else:
                for item in initial:
                    multipliermatch = _multiplierpattern.search(item)
                    if multipliermatch:
                        item = multipliermatch.group(1)
                        count = int(multipliermatch.group(2))
                        self.add(item, count)
                    else:
                        self.add(item)

    def add(self, item: str, count: int = 1):
        '''Add the item with a quantity.
        '''

        self._contents[item] = self._contents.get(item, 0) + count

    def remove(self, item: str, count: int = 1):
        '''Remove the item with a quantity.
        '''

        new = self._contents.get(item, 0) - count
        if new < 0:
            raise RuntimeError('Can not remove more of an item than we have')
        elif new == 0:
            del self._contents[item]
        else:
            self._contents[item] = new

    def __iadd__(self, other):
        if not isinstance(other, Equipment):
            raise RuntimeError('Can only add Equipment to equipment')
        for item, count in other._contents.items():
            self.add(item, count)

    def __add__(self, other) -> 'Equipment':
        # Just use iadd on a new instance
        new = Equipment(self)
        new += other
        return new

    def __iter__(self) -> Iterator[str]:
        return iter(self._contents)

    def __getitem__(self, key: str) -> int:
        return self._contents[key]

    def items(self) -> ItemsView[str, int]:
        return self._contents.items()
