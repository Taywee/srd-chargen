#!/usr/bin/env python3

from setuptools import setup
from pathlib import Path

# Get the current directory.  __file__ is the current running file's path, but
# is often a relative directory, so we use absolute() to get the full path and
# its parent, which is the directory itself.
this_dir = Path(__file__).absolute().parent

# Use the Path's overloaded __div__ method to build a child path.  This is done
# because path separators are different on different OSes.  Hardcoded paths can
# be frustrating to keep portable if you don't use a proper Path abstraction.
readme_path = this_dir / 'README.md'

# Here we actually read the entire readme into a string.
with readme_path.open() as file:
    long_description = file.read()

setup(
    name='srd-chargen',
    version='0.0.1',
    author='Taylor C. Richberger',
    description='A dnd srd character generator',
    long_description=long_description,
    long_description_content_type='text/markdown',
    license='GPLv3+',
    keywords=['dnd', 'dungeons', 'dragons', 'rpg', 'game', 'utility'],
    url='https://gitlab.com/Taywee/srd-chargen',
    entry_points=dict(
        console_scripts=[
            'chargen = chargen.__main__:main',
        ]
    ),
    packages=[
        'chargen',
    ],
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Operating System :: OS Independent',
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Topic :: Utilities',
        'Topic :: Games/Entertainment :: Role-Playing',
    ],
)
