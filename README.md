# srd-chargen

A simple DnD SRD character generator.

# Copyright

This project is copyright 2021 Taylor C. Richberger.

This project is not officially affiliated with Dungeons and Dragons or Wizards
of the Coast in any way.  All Dungeons and Dragons information present is taken
from the SRD, under the Open Gaming License.
